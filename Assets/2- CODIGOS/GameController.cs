﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour{
    public int numeroGemas;
    public int numeroMonedas;
    public Text textGemas;
    public Text textMonedas;

    void Start(){
        
    }

    public void publicarColecciones(){
        // Debug.Log permite visualizar texto en la consola se utiliza mucho prototipar y para monitorear.
        Debug.Log("numero gemas >>> " + numeroGemas);
        Debug.Log("numero monedas >>> " + numeroMonedas);
        textGemas.text = "Numero Gemas: " + numeroGemas;
        textMonedas.text = "Numero Monedas: " + numeroMonedas;
    }

    void Update(){
        
    }
}
