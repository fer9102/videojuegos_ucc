﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Vida : MonoBehaviour{
    public int saludInicial = 100;
    public int saludActual;
    public Slider barraSlider;
    private void Awake(){
        saludActual = saludInicial;
        
    }
    
    
    // Start is called before the first frame update
    void Start()
    {
    }

    public void TakeDaño (int valorDaño){
        saludActual = valorDaño;
        barraSlider.value = saludActual;

        if (saludActual <= 0)
        Destroy(gameObject);

    }

}

