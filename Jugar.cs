﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Jugar : MonoBehaviour{
    // declaro un metodo para ser invocado dentro del script
    public void IniciarJuego(){
        // utilizo metodo para cargar la escena de nivel 1
        SceneManager.LoadScene("level_01");
    }
}

