﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecogerGema : MonoBehaviour{

    public GameController controladorJuego;

    // Start is called before the first frame update
    void Start(){
        
    }

    void OnTriggerEnter(Collider objetoColisiona){
        if(objetoColisiona.tag == "ManoDerecha"){
            // destruir este objeto y sumar a la colección
            controladorJuego.numeroGemas++;
            controladorJuego.publicarColecciones();
            // destruyo el objeto
            Destroy(gameObject, 1.0f);
        }
    }

    // Update is called once per frame
    void Update(){
        
    }
}
