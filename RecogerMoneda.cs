﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecogerMoneda : MonoBehaviour{

    public GameController controladorJuego;

    // Start is called before the first frame update
    void Start()
    {

    }

    void OnTriggerEnter(Collider objetoColisiona)
    {
        if (objetoColisiona.tag == "Player")
        {
            // destruir este objeto y sumar a la colección
            controladorJuego.numeroMonedas++;
            controladorJuego.publicarColecciones();
            // destruyo el objeto
            Destroy(gameObject, 1.0f);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
